package com.codingchallenge.loopingvideo
import android.content.Context
import android.content.ContextWrapper
import androidx.core.content.ContentProviderCompat.requireContext
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import java.io.File
/*
    Singleton for a SimpleCacheInstance used by the VideoPlayerFragment
    This is because if multiple instances try write to the same cache location it will throw an exception
 */
public class SimpleCacheSingleton private constructor() {

    private var currentCache:SimpleCache? = null
    private fun getSimpleCacheFromContext(context:Context): SimpleCache
    {
        // cache settings
        val evictor = LeastRecentlyUsedCacheEvictor((100 * 1024 * 1024).toLong())
        val databaseProvider: DatabaseProvider = ExoDatabaseProvider(context)
        return SimpleCache(
            File(context.cacheDir, "media"),
            evictor,
            databaseProvider
        )
    }

    private object HOLDER {
        val INSTANCE = SimpleCacheSingleton()
    }

    companion object {
        val instance: SimpleCacheSingleton by lazy { HOLDER.INSTANCE }
    }


   /*
        We require context to select cacheDir + database provider
        A little bit kludgy as param is only used first time its called
        TODO workout cleaner way to get context into singleton
    */
   public fun getSimpleCache (context:Context): SimpleCache?
    {
        if(currentCache == null)
        {
            currentCache = getSimpleCacheFromContext(context)
        }

        return currentCache
    }

}